<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:atom="http://www.w3.org/2005/Atom">
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml" lang="en">
      <head>
        <title>
          <xsl:value-of select="/atom:feed/atom:title"/>
 | RSS Feed
        </title>
        <link rel="stylesheet" href="/lib/css/style.css"/>
        <style>
          hr{
            height:.5em;
            background-color: var(--lightest-green);
          }
          a {
            background-color: var(--light-green) !important;
            color: var(--black) !important;
            text-decoration: none;
          }
          a:hover {
            color: var(--lightest-green) !important;
            background-color: var(--dark-blue) !important;
            border-color: var(--lightest-green) !important;
          }
        </style>
      </head>
      <body>
        <h1>
          <xsl:value-of select="/atom:feed/atom:title"/>
 | RSS Feed
        </h1>
        <ul style="list-style:none;font-size:1.5em;">
          <li>
            <a href="/">back to proto.garden</a>
          </li>
          <li>
            <a href="/now/">back to now page</a>
          </li>
        </ul>

        <xsl:for-each select="/atom:feed/atom:entry">
          <article style="margin-top:1em;">
            <h2 style="font-size:2em;">
              <a>
                <xsl:attribute name="href">
                  <xsl:value-of select="atom:link/@href"/>
                </xsl:attribute>
                <xsl:value-of select="atom:title"/>
              </a>
            </h2>
            <hr/>
          </article>
        </xsl:for-each>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>